//
//  CustomCamera.swift
//  CustomCamera
//
//  Created by Giovanni Erpete on 28/02/2020.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import CoreMotion

class CustomCamera: UIView {
    
    var captureSession = AVCaptureSession()
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentCamera: AVCaptureDevice?
    var photoOutput: AVCapturePhotoOutput?
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    var cameraConnection: AVCaptureConnection?
    var outputPhoto : AVCapturePhoto?
    var image : UIImage?
    
    //
    let motion = CMMotionManager()
    var x = Double()
    var y = Double()
    var selected = false
    
    func setupCaptureSession() {
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
    }
    
    func setupDevice() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        let devices = deviceDiscoverySession.devices
        for device in devices {
            if device.position == AVCaptureDevice.Position.back {
                backCamera = device
            } else if device.position == AVCaptureDevice.Position.front {
                frontCamera = device
            }
            currentCamera = backCamera
        }
        
        checkISO()
        checkShutterSpeed()
        //1ma config
        var pIso : Int = 0
        if(!isos.contains(Float64(currentCamera!.iso))) {
            pIso = checkValue(Float64(currentCamera!.iso), isos)
        } else {
            var j = 0
            for i in isos {
                if(i == Float64(currentCamera!.iso)) {
                    pIso = j
                }
                j += 1
            }
        }
        var pSSpeed : Int = 0
        if(!speeds.contains(CMTimeGetSeconds(currentCamera!.exposureDuration))) {
            pSSpeed = checkValue(CMTimeGetSeconds(currentCamera!.exposureDuration), speeds)
        } else {
            var j = 0
            for i in speeds {
                if(i == CMTimeGetSeconds(currentCamera!.exposureDuration)) {
                    pSSpeed = j
                }
                j += 1
            }
        }
        let time = CMTimeMakeWithSeconds(speeds[pSSpeed], preferredTimescale: 1000000)
        do {
            try currentCamera!.lockForConfiguration()
            currentCamera!.focusMode = .autoFocus
            currentCamera!.setExposureModeCustom(duration: time, iso: Float(isos[pIso]), completionHandler: nil)
            currentCamera?.unlockForConfiguration()
        } catch {
            print("error")
        }
    }
    
    func setupInput() {
        do{
            if captureSession.inputs.isEmpty {
                let captureDeviceInput = try AVCaptureDeviceInput(device: currentCamera!)
                captureSession.addInput(captureDeviceInput)
            }
            
        } catch {
            print(error)
        }
    }
    
    func setOutput() {
        
        if captureSession.outputs.isEmpty {
            photoOutput = AVCapturePhotoOutput()
            photoOutput?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey : AVVideoCodecType.jpeg])], completionHandler: nil)
            captureSession.addOutput(photoOutput!)
        }
    }
    
    func setupPreviewLayer() {
        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        //        checkOrientation()
        cameraPreviewLayer!.frame = self.frame
        let rect = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        cameraPreviewLayer!.frame = rect
        self.layer.addSublayer(cameraPreviewLayer!)
        print ("PREV: \(cameraPreviewLayer)")
    }
    
    func startRunningCaptureSession() {
        captureSession.startRunning()
        cameraConnection = AVCaptureConnection(inputPort: captureSession.inputs.first!.ports.first!, videoPreviewLayer: cameraPreviewLayer!)
        print(cameraConnection!.debugDescription)
        checkRotation()
    }
    
    func checkRotation() {
        
        let interval = 0.01
        
        guard motion.isDeviceMotionAvailable else { return }
        motion.deviceMotionUpdateInterval = interval
        let queue = OperationQueue()
        motion.startDeviceMotionUpdates(to: queue, withHandler: {(data, error) in
            guard let data = data else { return }
            let gravity = data.gravity
            self.x = Double(round(100 * (gravity.x))/100)
            self.y = Double(round(100 * (gravity.y))/100)
            if(self.x > 0.8 && !self.selected) {
                if self.cameraConnection!.isVideoOrientationSupported {
                    self.selected = true
                    print("[CustomCamera] Landscape Right")
                    print("[CustomCamera] checkRotation: x: \(self.x) y: \(self.y)")
                    self.cameraConnection!.videoOrientation = .landscapeLeft
                }
            } else {
                self.selected = false
                self.cameraConnection!.videoOrientation = .portrait
            }
            if (self.x < -0.8 && !self.selected) {
                self.selected = true
                if self.cameraConnection!.isVideoOrientationSupported {
                    print("[CustomCamera] Landscape Left")
                    print("[CustomCamera] checkRotation: x: \(self.x) y: \(self.y)")
                    self.cameraConnection!.videoOrientation = .landscapeRight
                }
            } else {
                self.selected = false
                self.cameraConnection!.videoOrientation = .portrait
            }
        })
    }
    
    func checkValue(_ n: Float64, _ vect: [Float64])->Int {
        var d = Array<Float64>()
        for v in vect {
            d.append(n-v)
        }
        var min = d[0]
        var p = 0
        var i = 0
        for d1 in d {
            if(d1<min) {
                p = i
                min = d1
            }
            i += 1
        }
        return p
    }
    
    func setISO (_ valueIso: Float64, _ valueExposure: Float64) {
        let time = CMTimeMakeWithSeconds(valueExposure, preferredTimescale: 1000000)
        if(Float(valueIso) > (currentCamera?.activeFormat.minISO)! && Float(valueIso) < currentCamera!.activeFormat.maxISO){
            do {
                try currentCamera!.lockForConfiguration()
                currentCamera!.focusMode = .locked
                currentCamera!.setExposureModeCustom(duration: time, iso: Float(valueIso), completionHandler: nil)
                currentCamera?.unlockForConfiguration()
            } catch {
                print("error")
            }
        }else {
            print("[CustomCamera]: Can't do the operation (CHANGE ISO)")
        }
    }
    
    func changeExposureValue( _ valueIso: Float64, _ valueExposure: Float64) {
        let time = CMTimeMakeWithSeconds(valueExposure, preferredTimescale: 1000000)
        if(time > (currentCamera?.activeFormat.minExposureDuration)! && time < currentCamera!.activeFormat.maxExposureDuration){
            do {
                try currentCamera!.lockForConfiguration()
                currentCamera!.focusMode = .locked
                currentCamera!.setExposureModeCustom(duration: time, iso: Float(valueIso), completionHandler: nil)
                currentCamera?.unlockForConfiguration()
            } catch {
                print("error")
            }
        }else {
            print("[CustomCamera]: Can't do the operation (CHANGE SS)")
        }
    }
    
    func setFocus(_ focusPoint: CGPoint) {
        if let device = currentCamera {
            do {
                try device.lockForConfiguration()
                device.focusPointOfInterest = focusPoint
                device.focusMode = .autoFocus
                device.unlockForConfiguration()
            } catch {
                print("Error!")
            }
        }
    }
    
    func setZoom(_ scale: CGFloat) {
        if(scale < 1.0) {
            print("zoom = 1")
            if let device = currentCamera {
                do {
                    try device.lockForConfiguration()
                    device.videoZoomFactor = 1.0
                    device.unlockForConfiguration()
                } catch {
                    print("Error!")
                }
            }
            
        } else if (scale < 4.0) {
            //                metti lo zoom
            print("zoom: \(scale)")
            if let device = currentCamera {
                do {
                    try device.lockForConfiguration()
                    device.videoZoomFactor = scale
                    device.unlockForConfiguration()
                } catch {
                    print("Error!")
                }
            }
        }else {
            //                zoom uguale a 4
            print("zoom: 4")
            if let device = currentCamera {
                do {
                    try device.lockForConfiguration()
                    device.videoZoomFactor = 4.0
                    device.unlockForConfiguration()
                } catch {
                    print("Error!")
                }
            }
        }
    }
    
    func notifyDelegate() {
        setOutput()
        print("[Custom Camera]Notify delegate")
        let videoPreviewLayerOrientation = cameraConnection?.videoOrientation
        print("[CustomCamera]: videoOrientation: \(cameraConnection?.videoOrientation.rawValue)")
        if let photoOutputConnection = photoOutput?.connection(with: .video) {
            photoOutputConnection.videoOrientation = videoPreviewLayerOrientation!
        }
        let settings = AVCapturePhotoSettings()
        photoOutput!.capturePhoto(with: settings, delegate: self)
    }
    
    func savePhoto() {
        print("SAVE PHOTO")
        PHPhotoLibrary.shared().performChanges({
            let creationRequest = PHAssetCreationRequest.forAsset()
            print("[CustomCamera] data rapresentation: \(self.outputPhoto?.fileDataRepresentation())")
            creationRequest.addResource(with: .photo, data: (self.outputPhoto!.fileDataRepresentation())!, options: nil)
        }, completionHandler: { (complited, error) in
            if error != nil
            {
                print("Error")
            }
        })
    }
    
    func stopSession() {
        if captureSession.isRunning {
            
            print("[Custom Camera - Stop Session]: Stop session!")
            if let inputs = captureSession.inputs as? [AVCaptureDeviceInput] {
                for input in inputs {
                    print("[CustomCamera] sto rimuovendo gli input")
                    captureSession.removeInput(input)
                }
            }
            cameraPreviewLayer?.removeFromSuperlayer()
            if let outputs = captureSession.outputs as? [AVCaptureOutput] {
                for output in outputs {
                    
                    captureSession.removeOutput(output)
                }
            }
            captureSession.stopRunning()
        }
    }
    
    func checkISO(){
        var toDelete = [Int]()
        for i in 0...isos.count-1 {
            if(Float(isos[i]) < currentCamera!.activeFormat.minISO || Float(isos[i]) > currentCamera!.activeFormat.maxISO) {
                print("[CustomCamera] strunz, iso sballato: \(isos[i])")
                toDelete.append(i)
                //isos.remove(at: i)
            }
        }
        //
        //        for element in toDelete {
        //            isos.remove(at: element)
        //            isos.remove(at: isos.count - element)
        //        }
    }
    
    func checkShutterSpeed() {
        let min = CMTimeGetSeconds(currentCamera!.activeFormat.minExposureDuration)
        let max = CMTimeGetSeconds(currentCamera!.activeFormat.maxExposureDuration)
        var toDelete = [Int]()
        for i in 0...speeds.count-1 {
            if(speeds[i] < min || speeds[i] > max) {
                print("[CustomCamera]: shutterSpeed sballato: \(speedsString[i])")
            }
        }
        //        
        //        for i in toDelete {
        //            speeds.remove(at: i)
        //            speeds.remove(at: speeds.count - i)
        //            speedsString.remove(at: i)
        //            speedsString.remove(at: speeds.count - i)
        //        }
    }
}

extension CustomCamera: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let imageData = photo.fileDataRepresentation() {
            print(imageData)
            outputPhoto = photo
            print(photo.metadata)
            image = UIImage(data: imageData)
        }
    }
}
