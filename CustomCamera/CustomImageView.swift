//
//  CustomImageView.swift
//  CustomCamera
//
//  Created by Giovanni Erpete on 28/02/2020.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import UIKit
import Photos

class CustomImageView: UIImageView, PHPhotoLibraryChangeObserver {
    
    var images = [PHAsset]()
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        DispatchQueue.main.sync {
            print("Changed!")
            reloadCameraRoll()
        }
    }
    
    func checkPermission() {
        PHPhotoLibrary.requestAuthorization({
            (authorization) in
            if (authorization == .authorized) {
            }
        })
    }
    
    func loadCameraRoll () {
        if(PHPhotoLibrary.authorizationStatus() == .authorized) {
            let assets = PHAsset.fetchAssets(with: .image, options: nil)
            assets.enumerateObjects({(object, count, stop) in
                self.images.append(object)
                print(count)
            })
            self.images.reverse()
            let manager = PHImageManager.default()
            let asset = images.first!
            manager.requestImage(for: asset, targetSize: CGSize(width: self.frame.width, height: self.frame.height), contentMode: .aspectFit, options: nil, resultHandler: {(result, _) in
                self.image = result
            })
        }
    }
    
    func reloadCameraRoll() {
        self.images.reverse()
        let assets = PHAsset.fetchAssets(with: .image, options: nil)
        assets.enumerateObjects({(object, count, stop) in
            if(self.images.contains(object) == false) {
                self.images.append(object)
                print(count)
            }
        })
        self.images.reverse()
        let manager = PHImageManager.default()
        let asset = images.first!
        manager.requestImage(for: asset, targetSize: CGSize(width: self.frame.width, height: self.frame.height), contentMode: .aspectFit, options: nil, resultHandler: {(result, _) in
            
            self.image = result
        })
    }
    
    func setPhoto(_ n: Int) {
        let manager = PHImageManager.default()
        let asset = images.first!
        manager.requestImage(for: asset, targetSize: CGSize(width: self.frame.width, height: self.frame.height), contentMode: .aspectFit, options: nil, resultHandler: {(result, _) in
            self.image = result
        })
    }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
