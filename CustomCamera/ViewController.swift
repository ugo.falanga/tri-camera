//
//  ViewController.swift
//  CustomCamera
//
//  Created by Ugo Falanga on 13/02/2020.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

var isos : [Float64] = [25, 50, 100, 200, 400, 800, 1600, 2000, 1600, 800, 400, 200, 100, 50]
var speeds : [Float64] = [1/8000, 1/4000, 1/2000, 1/1000, 1/500, 1/250, 1/125, 1/60, 1/30, 1/15, 1/8, 1/4, 1/2, 1, 1/2, 1/4, 1/8, 1/15, 1/30, 1/60, 1/125, 1/250, 1/500, 1/1000, 1/2000, 1/4000]
var speedsString: [String] = ["1/8000", "1/4000", "1/2000", "1/1000", "1/500", "1/250", "1/125", "1/60", "1/30", "1/15", "1/8", "1/4", "1/2", "1", "1/2", "1/4", "1/8", "1/15", "1/30", "1/60", "1/125", "1/250", "1/500", "1/1000", "1/2000", "1/4000"]


import UIKit
import AVFoundation
import CoreMedia
import CoreMotion
import CoreHaptics
import Photos

let alphas : [CGFloat] = [0.2, 0.31, 0.42, 0.53, 0.64, 0.75, 0.86]

class ViewController: UIViewController {
    
    
    @IBOutlet weak var level: LevelView!
    
    @IBOutlet weak var customCamera: CustomCamera!
    
    //    knobs
    @IBOutlet weak var isoKnob: Knob!
    @IBOutlet weak var shutterSpeedKnob: Knob!
    
    @IBOutlet weak var aureaButton: UIButton!
    @IBOutlet weak var gridButton: UIButton!
    @IBOutlet weak var prospectiveButton: UIButton!
    @IBOutlet weak var levelButton: UIButton!
    
    
    //    camera stuff
    var image: UIImage?
    var outputPhoto: AVCapturePhoto?
    var value = 0
    var lastIso = isos[0]
    var vibra = false
    var lastShutter = 0
    
    //    stuff to prepare
    var iso: Float64 = 0
    var shutterSpeed : String = ""
    
    //    Button
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var isoImageView: UIImageView!
    @IBOutlet weak var tutorialButton: UIButton!
    //    labels
    @IBOutlet weak var isoLabel: UILabel!
    @IBOutlet weak var shutterSpeedLabel: UILabel!
    @IBOutlet weak var focalLabel: UILabel!
    @IBOutlet weak var focalMask: UIImageView!
    
    //    imageView
    @IBOutlet weak var lastImageView: CustomImageView!
    @IBOutlet weak var shutter1: UIImageView!
    @IBOutlet weak var shutter2: UIImageView!
    @IBOutlet weak var shutter3: UIImageView!
    @IBOutlet weak var shutter4: UIImageView!
    var a : CGFloat = 0
    var c : CGFloat = 0
    var lastValue : Float64 = isos[0]
    var d : CGFloat = 0
    var e : CGFloat = 0
    var final: CGFloat = 10
    var lastShutterSpeed = speeds[0]
    
    //    accelerometer stuff
    let motion = CMMotionManager()
    var x = Double()
    var y = Double()
    var selected = false
    var alreadyPinched = false
    
    //    buttons stuff
    var grayGridView: UIView?
    var grayGridImageView: UIImageView?
    var aureaGridView: UIView?
    var aureaGridImageView: UIImageView?
    var prospectiveGridView: UIView?
    var prospectiveGridImageView: UIImageView?
    var toggleState = 1
    var prospectiveTag = 1
    var aureaTag = 1
    var aureaNum = 0
    
    @IBOutlet weak var isoLabelPortrait: UILabel!
    @IBOutlet weak var shutterSpeedLabelPortrait: UILabel!
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.all
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("[VC] Dispappear!")
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isoKnob.setVect(isos)
        isoKnob.setOldValue(isos[0])
        shutterSpeedKnob.setVect(speeds)
        shutterSpeedKnob.setOldValue(speeds[0])
        customCamera.setupCaptureSession()
        customCamera.setupDevice()
        customCamera.setupInput()
        customCamera.setupPreviewLayer()
        customCamera.startRunningCaptureSession()
        customCamera.layer.addSublayer(level.layer)
        PHPhotoLibrary.shared().register(lastImageView)
        isoKnob.setMinimumValue(0)
        isoKnob.setMaximumValue(Float(isos.count - 1))
        isoKnob.addTarget(self, action: #selector(ViewController.handleValueChanged(_:)), for: .valueChanged)
        shutterSpeedKnob.setMinimumValue(0)
        shutterSpeedKnob.setMaximumValue(Float(speeds.count - 1))
        shutterSpeedKnob.addTarget(self, action: #selector(ViewController.handleValueChanged(_:)), for: .valueChanged)
        checkRotation()
        
        a = 1.0 - isoImageView.alpha
        c  = (a / CGFloat((isos.count ) / 2))
        d = CGFloat(Int((speeds.endIndex)))
        e = (final / ((d/2)+1))
        
        isoLabel.text = String(Int(isos[0]))
        shutterSpeedLabel.text = speedsString[0]
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(isActive), name: UIApplication.willResignActiveNotification, object: nil)
        let not = NotificationCenter.default
        not.addObserver(self, selector: #selector(isBackground), name: UIApplication.didBecomeActiveNotification, object: nil)
        
    }
    
    @objc func isActive() {
        print("[VC] I'm here!")
        isoKnob.stopHaptic()
        shutterSpeedKnob.stopHaptic()
        if level.levelTag == 2{
            level.stopHaptic()
        }
    }
    
    @objc func isBackground() {
        print("[VC]: I woke up!")
        isoKnob.haptic()
        shutterSpeedKnob.haptic()
        if level.levelTag == 2{
            level.haptic()
        }
    }
    
    @IBAction func handleValueChanged(_ sender: Any) {
        updateLabel()
        isoKnob.checkIsHaptic()
        shutterSpeedKnob.checkIsHaptic()
    }
    
    private func updateLabel() {
        isoLabel.text = "ISO \(String(Int(isos[Int(isoKnob.value)])))"
        shutterSpeedLabel.text = "\(speedsString[Int(shutterSpeedKnob.value)]) s"
        
        if (lastValue != isos[Int(isoKnob.value)]) {
            DispatchQueue.main.asyncAfter(deadline: .now()) { 
                self.isoLabelPortrait.alpha = 0.6
                self.isoLabelPortrait.text = self.isoLabel.text
            }
            
            customCamera.setISO(isos[Int(isoKnob.value)], speeds[Int(shutterSpeedKnob.value)])
            
            if (isoKnob.value < 7.0) {
                isoImageView.alpha = alphas[Int(isoKnob.value)]
            } else if (isoKnob.value > 8.0) {
                let n = Int(isoKnob.value) % alphas.count
                isoImageView.alpha = alphas[alphas.count - (n + 1)]
            } else {
                isoImageView.alpha = 1
            }
            lastValue = isos[Int(isoKnob.value)]
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.isoLabelPortrait.alpha = 0
            }
        }
        
        if (lastShutter != Int(shutterSpeedKnob.value)) {
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.shutterSpeedLabelPortrait.alpha = 0.6
                self.shutterSpeedLabelPortrait.text = self.shutterSpeedLabel.text
            }
            
            animatedShutter()
            customCamera.changeExposureValue(isos[Int(isoKnob.value)], speeds[Int(shutterSpeedKnob.value)])
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.shutterSpeedLabelPortrait.alpha = 0
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        lastImageView.checkPermission()
        lastImageView.loadCameraRoll()
        print("[VC] Will appear!")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let state = UIApplication.shared.applicationState
        if state == .background {
            print("[VC] background")
        }
        if state == .active {
            print("[VC] Active")
        }
    }
    
    
    @IBAction func touchDown(_ sender: Any) {
        customCamera.notifyDelegate()
        sleep(1)
    }
    
    @IBAction func cameraButton_TouchUpInside(_ sender: Any) {
        customCamera.savePhoto()
        iso = isos[Int(isoKnob.value)]
        shutterSpeed = speedsString[Int(shutterSpeedKnob.value)]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(level.levelTag == 2) {
            level.greyExImageView?.removeFromSuperview()
            level.greyImageView?.removeFromSuperview()
            level.alignedImageView?.removeFromSuperview()
            level.alignedYImageView?.removeFromSuperview()
            level.myTimer?.invalidate()
            level.stopHaptic()
            //            .setImage(UIImage(named: "disactlevel.png"), for: .normal)
            //            NB non cambia l'immagine perché il bottone è in LevelView
            
            level.levelTag = 1
        }
        
        if segue.identifier  == "PhotosPreview" {
            if let destinationVC = segue.destination as? PhotosPreviewViewController {
                destinationVC.iso = iso
                destinationVC.shutterSpeed = shutterSpeed
                destinationVC.customImageView = self.lastImageView
                
                //                self.dismiss(animated: true, completion: {print("Addio")})
            }
        }
    }
    
    @IBAction func isTapped(_ sender: UITouch) {
        let touchPoint = sender
        let screenSize = view.bounds.size
        //      rimuovo il vecchio mirino
        for v in customCamera.subviews {
            if v.tag == 3 {
                v.removeFromSuperview()
            }
        }
        //controllo se ho tappato nella view della camera
        if(touchPoint.location(in: view).y < customCamera.frame.maxY && touchPoint.location(in: view).y > customCamera.frame.minY) {
            let x = touchPoint.location(in: view).y / screenSize.height
            let y = touchPoint.location(in: view).x / screenSize.width
            let focusPoint = CGPoint(x: x, y: y)
            let rect = CGRect(x: touchPoint.location(in: view).x-60, y: touchPoint.location(in: view).y-150, width: 100.0, height: 100.0)
            let rectView = UIView(frame: rect)
            rectView.tag = 3
            let rectImage = UIImage(named: "Mirino.png")
            let rectImageView = UIImageView(image: rectImage)
            rectView.addSubview(rectImageView)
            customCamera.addSubview(rectView)
            customCamera.setFocus(focusPoint)
            let a = UIViewPropertyAnimator.init(duration: TimeInterval.init(exactly: 5.0)!, dampingRatio: 100) {
                rectView.alpha = 0.2
            }
            a.startAnimation(afterDelay: 2.0)
        }
    }
    
    @IBAction func isPinched(_ sender: UIPinchGestureRecognizer) {
        //      rimuovo il vecchio mirino
        for v in view.subviews {
            if v.tag == 3 {
                v.removeFromSuperview()
            }
        }
        if(sender.state == .changed) {
            if(sender.velocity > 0){
                customCamera.setZoom(sender.scale)
            } else {
                let newZoom = 4.0 - (4.0 - sender.scale)
                if(newZoom > 1 && !alreadyPinched) {
                    customCamera.setZoom(newZoom)
                } else if (newZoom < 1 && alreadyPinched) {
                    let newZoom = 4.0 - (4.0 - (sender.scale * 5))
                    customCamera.setZoom(newZoom)
                } else if (newZoom > 1 && alreadyPinched) {
                    customCamera.setZoom(newZoom)
                }
            }
        }
        if (sender.state == .ended && !alreadyPinched) {
            alreadyPinched = true
        }
    }
    
    @IBAction func gridBtn(_ sender: UIButton) {
        let third =  CGRect (x: 0, y: 0, width: customCamera.frame.width, height: customCamera.frame.height)
        for view in customCamera.subviews {
            if view.tag == 1 {
                view.removeFromSuperview()
                if prospectiveTag == 2 {
                    prospectiveButton.setImage(UIImage(named: "disactprospect.png"), for: .normal)
                    prospectiveTag = 1
                }
                if aureaTag == 2 {
                    aureaButton.setImage(UIImage(named: "disactaurea.png"), for: .normal)
                    aureaNum = 0
                    aureaTag = 1
                }
            }
        }
        if toggleState == 1 {
            grayGridView = UIView(frame: third)
            let grayGridImage = UIImage(named: "grid.png")
            grayGridImageView = UIImageView(image: grayGridImage)
            grayGridImageView?.contentMode = .scaleAspectFill
            grayGridImageView?.tag = 1
            customCamera!.addSubview(grayGridImageView!)
            sender.setImage(UIImage(named: "actgrid.png"), for: .normal)
            toggleState = 2
        } else {
            //                grayGridView!.removeFromSuperview()
            sender.setImage(UIImage(named: "disactgrid.png"), for: .normal)
            toggleState = 1
        }
    }
    
    @IBAction func aureaBtn(_ sender: UIButton) {
        for view in customCamera.subviews {
            if view.tag == 1 {
                view.removeFromSuperview()
                //                shutDownButtons()
                if prospectiveTag == 2 {
                    prospectiveButton.setImage(UIImage(named: "disactprospect.png"), for: .normal)
                    prospectiveTag = 1
                }
                if toggleState == 2 {
                    gridButton.setImage(UIImage(named: "disactgrid.png"), for: .normal)
                    toggleState = 1
                }
            }
        }
        aureaNum += 1
        
        let result = aureaNum % 5
        
        switch result {
        case 1:
            let image = UIImage(named: "sezaurea.png")
            let imageView = UIImageView(image: image)
            imageView.frame = level.frame
            imageView.tag = 1
            customCamera.addSubview(imageView)
            sender.setImage(UIImage(named: "actaurea1.png"), for: .normal)
            aureaTag = 2
        case 2:
            let image = UIImage(named: "sezaurea.png")
            let imageView = UIImageView(image: image)
            imageView.frame = level.frame
            imageView.tag = 1
            customCamera.addSubview(imageView)
            //            sender.imageView!.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))
            imageView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            sender.setImage(UIImage(named: "actaurea1p.png"), for: .normal)
        case 3:
            let image = UIImage(named: "sezaurea2.png")
            
            let imageView = UIImageView(image: image)
            imageView.frame = level.frame
            imageView.tag = 1
            customCamera.addSubview(imageView)
            sender.setImage(UIImage(named: "actaurea2.png"), for: .normal)
        case 4:
            let image = UIImage(named: "sezaurea2.png")
            let imageView = UIImageView(image: image)
            imageView.frame = level.frame
            imageView.tag = 1
            imageView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            customCamera.addSubview(imageView)
            sender.setImage(UIImage(named: "actaurea2p.png"), for: .normal)
        default:
            sender.setImage(UIImage(named: "disactaurea.png"), for: .normal)
            aureaTag = 1
        }
    }
    
    @IBAction func prospectiveBtn(_ sender: UIButton) {
        for view in customCamera.subviews {
            if view.tag == 1 {
                view.removeFromSuperview()
                if aureaTag == 2 {
                    aureaButton.setImage(UIImage(named: "disactaurea.png"), for: .normal)
                    aureaNum = 0
                    aureaTag = 1
                }
                if toggleState == 2 {
                    gridButton.setImage(UIImage(named: "disactgrid.png"), for: .normal)
                    toggleState = 1
                }
            }
        }
        if prospectiveTag == 1 {
            let prospectiveGridImage = UIImage(named: "grid2.png")
            prospectiveGridImageView = UIImageView(image: prospectiveGridImage)
            prospectiveGridImageView!.frame = level.frame
            prospectiveGridImageView!.tag = 1
            customCamera!.addSubview(prospectiveGridImageView!)
            sender.setImage(UIImage(named: "actprospect.png"), for: .normal)
            prospectiveTag = 2
        } else {
            //            prospectiveGridView!.removeFromSuperview()
            sender.setImage(UIImage(named: "disactprospect.png"), for: .normal)
            prospectiveTag = 1
        }
    }
    //    ANIMAZIONE ISO
    
    func animatedIso () {
        
        if(isos.count / 2 > Int(isoKnob.value)) {
            isoImageView.alpha += (c)
        }else {
            isoImageView.alpha = isoImageView.alpha - (c)
        }
        
    }
    
    //    ANIMAZIONE SHUTTER
    func animatedShutter(){
        var ne : CGFloat = 0
        print("[AS] shutterSpeedKnob Value: \(speeds[Int(shutterSpeedKnob.value)]), count: \(Int(shutterSpeedKnob.value))")
        if(lastShutterSpeed < speeds[Int(shutterSpeedKnob.value)] && Int(shutterSpeedKnob.value) != 0) {
            //            si devono allargare i 4 cerchi
            ne = e * CGFloat(Int(shutterSpeedKnob.value) + 1)
            if(ne <= 10) {
                print("[AS]1 Allarga di: \(ne)")
                animate(ne)
            } else {
                ne -= 10.0
                ne = 10.0 - ne
                print("[AS]2 Allarga di: \(ne)")
                animate(ne)
            }
            lastShutterSpeed = speeds[Int(shutterSpeedKnob.value)]
            lastShutter = Int(shutterSpeedKnob.value)
        }else if (lastShutterSpeed > speeds[Int(shutterSpeedKnob.value)] && Int(shutterSpeedKnob.value) != 0) {
            //            si devono stringere i 4 cerchi
            ne = e * CGFloat(Int(shutterSpeedKnob.value) + 1)
            if(ne <= 10) {
                print("[AS]1 Stringi di: \(ne)")
                animate(ne)
            } else {
                ne -= 10.0
                ne = 10.0 - ne
                print("[AS]2 Stringi di: \(ne)")
                animate(ne)
            }
            lastShutterSpeed = speeds[Int(shutterSpeedKnob.value)]
            lastShutter = Int(shutterSpeedKnob.value)
        }else if(Int(shutterSpeedKnob.value) == 0) {
            
            print("[AS] Posizione Iniziale!")
            lastShutter = Int(shutterSpeedKnob.value)
            animate(0)
        }
    }
    
    func animate(_ value: CGFloat) {
        shutter1.transform = CGAffineTransform(translationX: value, y: 0)
        shutter2.transform = CGAffineTransform(translationX: 0, y: value)
        shutter3.transform = CGAffineTransform(translationX: 0, y: -value)
        shutter4.transform = CGAffineTransform(translationX: -value, y: 0)
    }
    
    func checkRotation() {
        
        let interval = 0.01
        
        guard motion.isDeviceMotionAvailable else { return }
        motion.deviceMotionUpdateInterval = interval
        let queue = OperationQueue()
        motion.startDeviceMotionUpdates(to: queue, withHandler: {(data, error) in
            guard let data = data else { return }
            let gravity = data.gravity
            self.x = Double(round(100 * (gravity.x))/100)
            self.y = Double(round(100 * (gravity.y))/100)
            if(self.x > 0.7 && !self.selected) {
                self.selected = true
                //                    print("[ViewController] Landscape Right")
                //                    print("[ViewController] checkRotation: x: \(self.x) y: \(self.y)")
                self.rotation(-CGFloat.pi / 2)
            } else if (self.x < -0.7 && !self.selected) {
                self.selected = true
                //                    print("[ViewController] Landscape Left")
                //                    print("[ViewController] checkRotation: x: \(self.x) y: \(self.y)")
                self.rotation(CGFloat.pi / 2)
            }
            else if (self.x < 0.7 && self.x > -0.7 && self.selected){
                self.rotation(0)
                self.selected = false
                
            }
        })
    }
    
    func rotation(_ value: CGFloat) {
        DispatchQueue.main.sync {
            
            self.shutterSpeedLabel.setAnchorPoint(CGPoint(x: 0.5, y: 0))
            self.focalLabel.setAnchorPoint(CGPoint(x: 0.5, y: 0))
            self.isoLabel.setAnchorPoint(CGPoint(x: 0.5, y: 0))
            UIView.animate(withDuration: 0.25, animations: {
                if self.aureaTag != 2 {
                    self.aureaButton.transform = CGAffineTransform(rotationAngle: value)
                    //self.aureaButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
                }
                self.gridButton.transform = CGAffineTransform(rotationAngle: value)
                self.prospectiveButton.transform = CGAffineTransform(rotationAngle: value)
                self.levelButton.transform = CGAffineTransform(rotationAngle: value)
                self.shutterSpeedLabel.transform = CGAffineTransform(rotationAngle: value)
                self.focalLabel.transform = CGAffineTransform(rotationAngle: value)
                self.isoLabel.transform = CGAffineTransform(rotationAngle: value)
                self.lastImageView.transform = CGAffineTransform(rotationAngle: value)
                self.tutorialButton.transform = CGAffineTransform(rotationAngle: value)
                self.focalMask.transform = CGAffineTransform(rotationAngle: value)
                self.isoLabelPortrait.transform = CGAffineTransform(rotationAngle: value)
                self.shutterSpeedLabelPortrait.transform = CGAffineTransform(rotationAngle: value)
            })
        }
    }
}

extension UIView {
    func setAnchorPoint(_ point: CGPoint) {
        var newPoint = CGPoint(x: bounds.size.width * point.x, y: bounds.size.height * point.y)
        var oldPoint = CGPoint(x: bounds.size.width * layer.anchorPoint.x, y: bounds.size.height * layer.anchorPoint.y);
        newPoint = newPoint.applying(transform)
        oldPoint = oldPoint.applying(transform)
        var position = layer.position
        position.x -= oldPoint.x
        position.x += newPoint.x
        position.y -= oldPoint.y
        position.y += newPoint.y
        layer.position = position
        layer.anchorPoint = point
    }
}

