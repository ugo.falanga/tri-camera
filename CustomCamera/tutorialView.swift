//
//  tutorialView.swift
//  CustomCamera
//
//  Created by Mirella Cetronio on 08/03/2020.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import UIKit

class tutorialView: UIViewController {
    
    
    @IBOutlet weak var focalBtn: UIButton!
    @IBOutlet weak var shutterBtn: UIButton!
    @IBOutlet weak var isoBtn: UIButton!
    
    @IBOutlet weak var isoAnimation: UIImageView!
    
    @IBOutlet weak var shutter1: UIImageView!
    @IBOutlet weak var shutter2: UIImageView!
    @IBOutlet weak var shutter3: UIImageView!
    @IBOutlet weak var shutter4: UIImageView!
    
    @IBOutlet weak var focalTutorial1: UIImageView!
    @IBOutlet weak var focalTutorial2: UIImageView!
    @IBOutlet weak var focalTutorial3: UIImageView!
    @IBOutlet weak var focalTutorial4: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tutorialLabel: UITextView!
    
    @IBOutlet weak var triangleOrizontal: UIImageView!
    @IBOutlet weak var triangleDx: UIImageView!
    @IBOutlet weak var triangleSx: UIImageView!
    
    var tagState = 0

    var tutorialText: String = "Aperture, shutter speed and ISO make up the three sides of the exposure triangle.\n\nThey Work together to produce a photo that is properly exposed. If one variable changes, at least one of the others must also change to mantain the correct exposure.\n\nFor more information on each vertex of the triangle, tap on the matching icon."
    var apertureText: String = "Aperture refers to the size of the circular hole in the lens that lets in light. The bigger the hole, the more light that reaches the sensor. In fact, each time you double the area of that opening, you double the amount of light or increase the exposure by one stop. On the other hand, if you half the area of the opening, you half the amount of light hitting the sensor. And you guessed it; that will decrease the exposure by one stop.\n\nNow without getting too technical, an f-stop is a ratio that relates to the size of that opening. Diameter is equal to focal length/f-stop, this means that large f-stop numbers refer to small openings and small f-stop numbers refer to large openings.\n\nUnfortunately it is not possible to change this value on iPhone."
    var shutterspeedlText: String = "Shutter speed is the length of time light is allowed to hit the sensor. It is measured in seconds. Shutter speed is probably the easiest of the exposure triangle sides to understand.\n\nTo double the amount of light, we need to double the length of the exposure. For example, moving from a shutter speed of 1⁄60 s to 1⁄30 s will add a stop of light because the shutter will remain open twice as long. Changing from a shutter speed of 1s to 1/8 s will decrease the exposure by three stops. Why? From 1s to 1⁄2 s is one stop. Then 1⁄2 s to 1⁄4 s is another stop. Finally, 1⁄4 s to 1⁄8 s is a further halving of the time the shutter remains open or the third stop."
    var isoText: String = "ISO is one of the variables in the exposure triangle. You can think of ISO as the sensitivity of the digital sensor (although it is a lot more complicated than that). Higher values of ISO mean that the sensor does not need to collect as much light to make a correct exposure. Low ISO values mean that the sensor will have to gather more light to make the exposure.\n\nLike shutter speed, this ISO scale is easy to understand. Doubling the ISO equates to a one stop increase in exposure. Halving the ISO leads to a reduction of the exposure by one stop."
    
    override func viewDidLoad() {
        super.viewDidLoad()
// Do any additional setup after loading the view.
        self.tutorialLabel.text = tutorialText
//        self.drawTriangleOriz ()
//        self.drawTriangleSx ()
//        self.drawTriangleDx ()
    }
    
    @IBAction func focal(_ sender: UIButton) {
        
        self.isoBtn.isEnabled = false
        self.focalBtn.isEnabled = false
        self.shutterBtn.isEnabled = false
        Timer.scheduledTimer(withTimeInterval: 2.1, repeats: false) {
            [weak self]timer in
            self?.isoBtn.isEnabled = true
            self?.focalBtn.isEnabled = true
            self?.shutterBtn.isEnabled = true
        }
        
        self.text("", 0.25, "")
        
        if tagState != 1 {
            
            if tagState == 2 || tagState == 3 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.1) {
                    self.text("Aperture", 0.25, "\(self.apertureText)")
                }
                self.isoAlpha(1.0, 1.0, 0.25)
                self.focalTri(1.0, 2.0, 1.2, 1.5, 2.3)
                self.shutterTriTransform (1.0, 1.0, 0, 0)
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.1) {
                    self.text("Aperture", 0.25, "\(self.apertureText)")
                }
                self.isoAlpha(1.0, 0, 0.25)
                self.focalTri(1.0, 0, 1.2, 1.5, 2.3)
                self.shutterTriTransform (1.0, 0, 0, 0)
            }
            
//            ANIMAZIONE TRIANGOLO
//            Posizione iniziale
            if tagState == 3 {
                self.stretchTriangleOrizontal(0.25, 0, 0.5, 0, 1, 1)
            } else {
                self.stretchTriangleOrizontal(0.25, 1, 0.5, 0, 1, 1)
            }
            self.stretchTriangleDx(0.25, 0, 0, 0, 1, 1)
            self.stretchTriangleSx(0.25, 1, 0, 0, 1, 1)
            
//            Posizione finale
            if tagState == 0 {
                self.stretchTriangleDx(1, 1, 1, 9.8, 1.4, 1.28)
                self.stretchTriangleSx(1, 0, 1, 0, 1, 1.4)
            } else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.1) {
                    self.stretchTriangleDx(1, 1, 1, 9.8, 1.4, 1.28)
                    self.stretchTriangleSx(1, 0, 1, 0, 1, 1.4)
                }
            }
            self.tagState = 1
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.text("The Exposure Triangle", 0.25, "\(self.tutorialText)")
            }
            self.stretchTriangleOrizontal(0.25, 0, 0.5, 0, 1, 1)
            self.stretchTriangleDx(0.25, 1, 1, 0, 1, 1)
            self.stretchTriangleSx(0.25, 0, 1, 0, 1, 1)
            self.focalTri(1.0, 1.0, 1, 1, 1)
            tagState = 0
        }
    }
    
    @IBAction func shutter(_ sender: UIButton) {
        
        self.isoBtn.isEnabled = false
        self.focalBtn.isEnabled = false
        self.shutterBtn.isEnabled = false
        Timer.scheduledTimer(withTimeInterval: 2.1, repeats: false) {
            [weak self]timer in
            self?.isoBtn.isEnabled = true
            self?.focalBtn.isEnabled = true
            self?.shutterBtn.isEnabled = true
        }
        
        self.text("", 0.25, "")
        
        if tagState != 2 {
            
            if tagState == 1 || tagState == 3 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.1) {
                    self.text("Shutter Speed", 0.25, "\(self.shutterspeedlText)")
                }
                self.isoAlpha(1.0, 1.0, 0.25)
                self.focalTri(1.0, 1.0, 1, 1, 1)
                self.shutterTriTransform (1.0, 2.0, 0, 10)
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.1) {
                    self.text("Shutter Speed", 0.25, "\(self.shutterspeedlText)")
                }
                self.isoAlpha(1.0, 0, 0.25)
                self.focalTri(1.0, 0, 1, 1, 1)
                self.shutterTriTransform(1.0, 0, 0, 10)
            }
            
//            ANIMAZIONE TRIANGOLO
//            Posizione iniziale
            self.stretchTriangleOrizontal(0.25, 0, 0.5, 0, 1, 1)
            if tagState == 3 {
                self.stretchTriangleDx(0.25, 0, 0, 0, 1, 1)
            } else {
                self.stretchTriangleDx(0.25, 1, 1, 0, 1, 1)
            }
            self.stretchTriangleSx(0.25, 0, 1, 0, 1, 1)
            
            //            Posizione finale
            if tagState == 0 {
                self.stretchTriangleOrizontal(1, 1, 0.5, -8.20, 1.37, 1)
                self.stretchTriangleSx (1, 1, 0, 17.5, 1, 1.47)
            } else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.1) {
                    self.stretchTriangleOrizontal(1, 1, 0.5, -8.20, 1.37, 1)
                    self.stretchTriangleSx (1, 1, 0, 17.5, 1, 1.47)
                }
            }
            self.tagState = 2
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.text("The Exposure Triangle", 0.25, "\(self.tutorialText)")
            }
            self.stretchTriangleOrizontal(0.25, 1, 0.5, 0, 1, 1)
            self.stretchTriangleDx(0.25, 0, 0, 0, 1, 1)
            self.stretchTriangleSx(0.25, 1, 0, 0, 1, 1)
            self.shutterTriTransform(1.0, 1.0, 0, 0)
            tagState = 0
        }
    }
    
    @IBAction func iso(_ sender: UIButton) {
        
        self.isoBtn.isEnabled = false
        self.focalBtn.isEnabled = false
        self.shutterBtn.isEnabled = false
        Timer.scheduledTimer(withTimeInterval: 2.1, repeats: false) {
            [weak self]timer in
            self?.isoBtn.isEnabled = true
            self?.focalBtn.isEnabled = true
            self?.shutterBtn.isEnabled = true
        }
        
        self.text("", 0.25, "")
        
        if tagState != 3 {
            
            if tagState == 1 || tagState == 2 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.1) {
                    self.text("ISO", 0.25, "\(self.isoText)")
                }
                self.isoAlpha(1.0, 2.0, 1)
                self.focalTri(1.0, 1.0, 1, 1, 1)
                self.shutterTriTransform (1.0, 1.0, 0, 0)
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.1) {
                    self.text("ISO", 0.25, "\(self.isoText)")
                }
                self.isoAlpha (1.0, 0, 1)
                self.focalTri(1.0, 0, 1, 1, 1)
                self.shutterTriTransform(1.0, 0, 0, 0)
            }
            
//            ANIMAZIONE TRIANGOLO
//            Posizione iniziale
            self.stretchTriangleOrizontal(0.25, 1, 0.5, 0, 1, 1)
            self.stretchTriangleDx(0.25, 1, 1, 0, 1, 1)
            if tagState == 2{
                self.stretchTriangleSx(0.25, 1, 0, 0, 1, 1)
            } else {
                self.stretchTriangleSx(0.25, 1, 1, 0, 1, 1)
            }
            
//            Posizione finale
            if tagState == 0 {
                self.stretchTriangleOrizontal(1, 0, 0.5, 8.3, 1.4, 1)
                self.stretchTriangleDx(1, 0, 0, -10, 1.4, 1.4)
            } else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.1) {
                    self.stretchTriangleOrizontal(1, 0, 0.5, 8.3, 1.4, 1)
                    self.stretchTriangleDx(1, 0, 0, -10, 1.4, 1.4)
                }
            }
            tagState = 3
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.text("The Exposure Triangle", 0.25, "\(self.tutorialText)")
            }
            self.stretchTriangleOrizontal(0.25, 0, 0.5, 0, 1, 1)
            self.stretchTriangleDx(0.25, 0, 0, 0, 1, 1)
            self.stretchTriangleSx(0.25, 0, 1, 0, 1, 1)
            self.isoAlpha(1.0, 1.0, 0.25)
            tagState = 0
        }
    }
    
    
    func drawTriangleOriz (){
        self.drawLine(startX: 150, toEndingX: 265, startingY: 311, toEndingY: 311, ofColor: UIColor.white, widthOfLine: 1)
    }
    
    func drawTriangleSx (){
        self.drawLine(startX: 150, toEndingX: 207.5, startingY: 311, toEndingY: 211.41, ofColor: UIColor.white, widthOfLine: 1)
    }
    
    func drawTriangleDx (){
        self.drawLine(startX: 265, toEndingX: 207.5, startingY: 311, toEndingY: 211.41, ofColor: UIColor.white, widthOfLine: 1)
    }
    
//    funzioni usate
    
    func drawLine(startX: Double, toEndingX endX: Double, startingY startY: Double, toEndingY endY: Double, ofColor lineColor: UIColor, widthOfLine lineWidth: CGFloat) {
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: startX, y: startY))
        path.addLine(to: CGPoint(x: endX, y: endY))
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = lineColor.cgColor
        shapeLayer.lineWidth = lineWidth
        
        view.layer.addSublayer(shapeLayer)
    }
    
    func text (_ titleLabelText: String, _ animationDuration: Double, _ textTutorial: String) {
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.subtype = CATransitionSubtype.fromTop
        self.titleLabel.text = titleLabelText
        self.tutorialLabel.text = textTutorial
        animation.duration = animationDuration
        self.titleLabel.layer.add(animation, forKey: CATransitionType.fade.rawValue)
        self.tutorialLabel.layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
    
    func focalTri (_ time: Double, _ delay: Double, _ scale2: Double, _ scale3: Double, _ scale4: Double) {
        UIView.animate(withDuration: time, delay: delay, animations: {
            self.focalTutorial2.transform = CGAffineTransform(scaleX: CGFloat(scale2), y: CGFloat(scale2))
            self.focalTutorial3.transform = CGAffineTransform(scaleX: CGFloat(scale3), y: CGFloat(scale3))
            self.focalTutorial4.transform = CGAffineTransform(scaleX: CGFloat(scale4), y: CGFloat(scale4))
        })
    }
    
    func isoAlpha (_ time: Double, _ delay: Double, _ value :Double){
        UIView.animate(withDuration: time, delay: delay, animations: {
            self.isoAnimation.alpha = CGFloat(value)
        })
    }
    
    func shutterTriTransform (_ time: Double, _ delay: Double, _ value :Double, _ value2 :Double){
        UIView.animate(withDuration: time, delay: delay, animations: {
            self.shutter1.transform = CGAffineTransform(translationX: CGFloat(value), y: CGFloat(value2))
            self.shutter2.transform = CGAffineTransform(translationX: CGFloat(-value2), y: CGFloat(value))
            self.shutter3.transform = CGAffineTransform(translationX: CGFloat(value), y: CGFloat(-value2))
            self.shutter4.transform = CGAffineTransform(translationX: CGFloat(value2), y: CGFloat(value))
        })
    }
    
    func stretchTriangleOrizontal (_ alpha: CGFloat, _ anchorX: Double, _ anchorY: Double, _ angle: Double, _ scaleX: Double, _ scaleY: Double){
        self.triangleOrizontal.anchorPoint(CGPoint(x: anchorX, y: anchorY))
        UIView.animate(withDuration: 1.0){
            self.triangleOrizontal.alpha = alpha
            var stretch = CGAffineTransform.identity
            stretch = stretch.rotated(by: CGFloat.pi * CGFloat((angle/180)))
            stretch = stretch.scaledBy(x: CGFloat(scaleX), y: CGFloat(scaleY))
            self.triangleOrizontal.transform = stretch
        }
    }
    
    func stretchTriangleSx (_ alpha: CGFloat, _ anchorX: Double, _ anchorY: Double, _ angle: Double, _ scaleX: Double, _ scaleY: Double){
        self.triangleSx.anchorPoint(CGPoint(x: anchorX, y: anchorY))
        UIView.animate(withDuration: 1.0){
            self.triangleSx.alpha = alpha
            var stretch = CGAffineTransform.identity
            stretch = stretch.rotated(by: CGFloat.pi * CGFloat((angle/180)))
            stretch = stretch.scaledBy(x: CGFloat(scaleX), y: CGFloat(scaleY))
            self.triangleSx.transform = stretch
        }
    }
    
    func stretchTriangleDx (_ alpha: CGFloat, _ anchorX: Double, _ anchorY: Double, _ angle: Double, _ scaleX: Double, _ scaleY: Double){
        self.triangleDx.anchorPoint(CGPoint(x: anchorX, y: anchorY))
        UIView.animate(withDuration: 1.0){
            self.triangleDx.alpha = alpha
            var stretch = CGAffineTransform.identity
            stretch = stretch.rotated(by: CGFloat.pi * CGFloat((angle/180)))
            stretch = stretch.scaledBy(x: CGFloat(scaleX), y: CGFloat(scaleY))
            self.triangleDx.transform = stretch
        }
    }
}

extension UIView {
    func anchorPoint(_ point: CGPoint) {
        var newPoint = CGPoint(x: bounds.size.width * point.x, y: bounds.size.height * point.y)
        var oldPoint = CGPoint(x: bounds.size.width * layer.anchorPoint.x, y: bounds.size.height * layer.anchorPoint.y);
        
        newPoint = newPoint.applying(transform)
        oldPoint = oldPoint.applying(transform)
        
        var position = layer.position
        
        position.x -= oldPoint.x
        position.x += newPoint.x
        
        position.y -= oldPoint.y
        position.y += newPoint.y
        
        layer.position = position
        layer.anchorPoint = point
    }
}


