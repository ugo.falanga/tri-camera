//
//  PhotosPreviewViewController.swift
//  CustomCamera
//
//  Created by Giovanni Erpete on 29/02/2020.
//  Copyright © 2020 Apple Inc. All rights reserved.
//

import UIKit

class PhotosPreviewViewController: UIViewController {
    
    @IBOutlet weak var customImageView: CustomImageView!
    //PrepareStuff
    var iso: Float64!
    var shutterSpeed: String!
    var n = 0
    
    @IBOutlet weak var isoLabel: UILabel!
    @IBOutlet weak var focalLabel: UILabel!
    @IBOutlet weak var shutterLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isoLabel.text = "ISO \(String(Int(iso)))"
        shutterLabel.text = "\(String(shutterSpeed)) s"
        //        customImageView.loadCameraRoll()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        customImageView.loadCameraRoll()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}
